using System;
using System.Collections.Generic;
using System.Linq;
using CalenderContainer;
using System.Xml.Linq;
using System.IO;
using Windows.Storage;

namespace MemoryManagement
{

    class Storage
    {   
        static private XDocument Database;
        static private string FileName;
        static StorageFile file;

        static public void InitDocument()
        {
            StorageFolder Folder = ApplicationData.Current.LocalFolder;
            file = Folder.CreateFileAsync("DataBase.xml",CreationCollisionOption.OpenIfExists).GetAwaiter().GetResult();
            Folder.GetFileAsync("").GetAwaiter().GetResult();
            FileName = file.Path;
            string data = FileIO.ReadTextAsync(file).GetAwaiter().GetResult();
            
            // Initialize XML file at first start
            if(data == string.Empty )
                data = "<?xml version=\"1.0\" encoding=\"UTF - 8\" standalone=\"yes\" ?> <Root></Root>";

            Database = XDocument.Parse(data);
        }
        
        static public void WriteDay(Day Data)
        {
            string Datestring = Data.Date; // Key

            IEnumerable<XElement> RootPath = Database.Root.Elements("Day");

            if(RootPath.Any(x => (string)x.Attribute("Date") == Datestring))
                RootPath.Where(
                    Day => (string)Day.Attribute("Date") == Datestring)
                    .Remove(); // L�schen des alten wertes

            Database.Root.Add(Data.ToXElement());
            var Stream = file.OpenStreamForWriteAsync().GetAwaiter().GetResult();
            Database.Save(Stream); // sichern in der XML File
        }

        /// <summary>
        /// Returns the Day with the specified Date
        /// (returns null when no Day with that date is registered)
        /// </summary>
        /// <param name="Date">Specified Date of the Day wich should be returned</param>
        /// <returns></returns>
        static public Day ReadDay(string Date)
        {
            string Datestring = Date; 
            IEnumerable<XElement> RootPath = Database.Root.Elements("Day") ;

            XElement Day = null;
            if(RootPath.Any( x => (string)x.Attribute("Date") == Datestring))
              Day = RootPath.Single(day => (string)day.Attribute("Date") == Datestring); // Entnimmt den Tag mit
                                                                                         // dem bestimmten Datum
            // null wenn kein Element mit gew�nschtem Attribute vorhanden
            return Day == null ? null : new Day(Day);
        }
    }
}