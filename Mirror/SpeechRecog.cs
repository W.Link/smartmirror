﻿using System;
using Windows.Media.SpeechRecognition;
using Windows.UI.Core;
using AudioFeedback;

namespace Mirror
{
  class SpeechRecog
  {
    private SpeechRecognitionListConstraint grammar;
    private SpeechRecognitionListConstraint wakegrammar;
    private Voice MirrorsVoice;
    // Websearch for words (for example for calendar entries)
    //private SpeechRecognitionTopicConstraint webGrammar;
    private SpeechRecognizer CommandRec;
    private SpeechRecognizer WakeUpRec;
    CoreDispatcher Dispatcher;

    string[] WakeUpCalls = { "Mirror", "Spiegel","Temperatur","Kalender"};
    string[] grammarList = { "Zeig das Wetter an","Schließe das Wetter","Temperatur"};
    Action[] Commands = { MainPage.SetWeatherCollapsed, MainPage.SetWeatherVisible };


    public SpeechRecog(Uri grammarUri,CoreDispatcher Dispatch)
    { 
      Dispatcher = Dispatch;
      MirrorsVoice = new Voice();

      grammar = new SpeechRecognitionListConstraint(grammarList);
      wakegrammar = new SpeechRecognitionListConstraint(WakeUpCalls);

      CommandRec = new SpeechRecognizer();
      WakeUpRec = new SpeechRecognizer();

      CommandRec.Constraints.Add(grammar);
      WakeUpRec.Constraints.Add(wakegrammar);

      // Compile Constrains
      CommandRec.CompileConstraintsAsync().GetAwaiter().GetResult();
      WakeUpRec.CompileConstraintsAsync().GetAwaiter().GetResult();
    }
    /// <summary>
    /// Listens always for the 
    /// </summary>
    public void start()
    {
      SpeechRecognitionResult result;
      while (true)
      {
        result = WakeUpRec.RecognizeAsync().GetAwaiter().GetResult();
        //if (result.Confidence == SpeechRecognitionConfidence.High)
          if (result.Text == "Mirror" || result.Text == "Spiegel")
          {
            ListenForCommand();
          }
          else if (result.Text == "Temperatur")
            Dispatcher.RunAsync(CoreDispatcherPriority.High, MirrorsVoice.SayWeather);
          else if (result.Text == "Kalender")
            Dispatcher.RunAsync(CoreDispatcherPriority.High, MainPage.ChangeCalenderVisibility);

      }
    }

    private void ListenForCommand()
    {
      SpeechRecognitionResult result = CommandRec.RecognizeAsync().GetAwaiter().GetResult();
      if (result.Confidence == SpeechRecognitionConfidence.High)
      {
        switch (result.Text)
        {
          case "Zeig das Wetter an":
            Dispatcher.RunAsync(CoreDispatcherPriority.High, MainPage.SetWeatherCollapsed);
            break;
          case "Schließe das Wetter":
            Dispatcher.RunAsync(CoreDispatcherPriority.High, MainPage.SetWeatherCollapsed);
            break;
          case "Temperatur":
            Dispatcher.RunAsync(CoreDispatcherPriority.High, MirrorsVoice.SayWeather);
            break;
        }
      }
    }
  }
}
