﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Windows.UI.Xaml.Controls;
using Windows.Web.Http;
using System.Xml.Linq;
using System.Diagnostics;
using System.Net.NetworkInformation;
using Windows.Data.Xml.Dom;

namespace Mirror
{
  satic class Weather
  {
    static string Temperature;
    static string Condition;
    static string Code;
    static string City;
    static string Country;
    static string woeid = "684946";
    static weatherDataContainer Data = new weatherDataContainer();

    public struct weatherDataContainer
    {
      public string Temperature;
      public string Condition;
      public string City;
      public string Country;
      public Uri picture;
    }
    public static weatherDataContainer getWeatherData()
    {
      String URL = String.Format(@"http://query.yahooapis.com/v1/public/yql?format=xml&q=select+*+from+weather.forecast+where+woeid=");   // URL um die Daten zu Laden
      URL = URL + woeid;
      // In Uri wandeln
      Uri uri = new Uri(URL);

      HttpClient Client = new HttpClient();

      string Data;

      try
      {
        Data = Client.GetStringAsync(uri).GetAwaiter().GetResult();
        XMLstringRead Reader = new XMLstringRead(Data);

        Country = Reader.GetAttfromElem("yweather:location", "country");
        City = Reader.ReadAttributeValue("city");
        Temperature = Reader.GetAttfromElem("yweather:condition", "temp");
        Condition = Reader.ReadAttributeValue("text");
        Code = Reader.ReadAttributeValue("code");
      }
      catch (Exception ex)
      {
        Debug.WriteLine(ex.Message);
        Debug.WriteLine(ex.StackTrace);
      }

      try
      {
        var httpClient = new HttpClient();
        var httpResponse = httpClient.GetAsync(uri).GetAwaiter().GetResult();
        httpResponse.EnsureSuccessStatusCode();
        var httpResponseBody = httpResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
        weatherDataXml.LoadXml(httpResponseBody);
        IXmlNode channel = weatherDataXml.SelectSingleNode("query").SelectSingleNode("results").SelectSingleNode("channel");  //bestimmten node auswählen
        XmlNodeList item = channel.SelectNodes("item");

        foreach (var node in item)
        {
          var attr = node.Attributes;
        }
        //var Temperature = item.SelectSingleNode("yweather:condition").Attributes;
        //Condition = item.SelectSingleNode("yweather:condition").Attributes[4].NodeValue.ToString();
        //Code = item.SelectSingleNode("yweather:condition").Attributes[1].NodeValue.ToString();
        //Town = channel.SelectSingleNode("yweather:location").Attributes[1].NodeValue.ToString();
        //Country = channel.SelectSingleNode("yweather:location").Attributes[2].NodeValue.ToString();

        //Data.Temperature = FahrenheitToCelsius(Temperature);
        Data.Town = Town;
        Data.Country = Country;
        Data.Condition = Condition;
        Data.picture = DefineIconPath();
      }
      catch (Exception ex)
      {
        Debug.WriteLine(ex.Message);
        Debug.WriteLine(ex.StackTrace);
      }
      return Data;
    }

    static private string FahrenheitToCelsius(string Fahrenheit)
    {
      double x = (Double.Parse(Fahrenheit) - 32) * 5.0 / 9.0;
      x = (int)x;
      return x.ToString() + " " + string.Format("\u00B0") + "C";
    }

    private static Uri DefineIconPath()
    {
      Uri path;
      if (Code.Equals("3200"))
        path = new Uri("ms-appx:///Assets/na.png");
      else
        path = new Uri(@"ms-appx:///Assets/" + Code + ".png");

      return path;
    }
  }
}

