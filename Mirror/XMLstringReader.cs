﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mirror
{ 
  /// <summary>
  /// Reads an XML as string and can searche for elements and attributes
  /// (needs to be rewind)
  /// </summary>
  class XMLstringRead
  {
    /// <summary>
    /// Position of the pointer in the XML
    /// </summary>
    int Position;
    string XML;
    /// <summary>
    /// Shows in wich element the pointer currently is positioned
    /// </summary>
    string CurrentElement;

    public string GetCurrentElement()
    {
      return CurrentElement;
    }
    /// <summary>
    /// Initialize the XML string wich schould be read
    /// </summary>
    /// <param name="XML"></param>
    public XMLstringRead(string XML)
    {
      this.XML = XML;
      Position = 0;
    }

    /// <summary>
    /// Search for first Element with defiened name and returns the value from the specified Attribute
    /// </summary>
    /// <param name="Element">name of the element</param>
    /// <param name="Attribute">name of the attribute</param>
    /// <returns></returns>
    public string GetAttfromElem(string Element, string Attribute)
    {
      string Result = null;
      while(XML[Position] != '\0')
      {
        // Read until next element is found and then jump over '<'
        while (XML[Position++] != '<') ;

        // read name of the element and then jump over ' ' (blank)
        string Readelement = string.Empty;
        do
        {
          Readelement += XML[Position++];
        }
        while (XML[Position] != ' ' && XML[Position] != '>');

        // Position++ to jump over the " " blank
        Position++;
        if (Readelement == Element)
        {
          // Saves on wich element it ist currently pointing
          CurrentElement = Readelement;
          // Searches the Attribute and returns the value
          Result = ReadAttributeValue(Attribute);
          break;
        }
      }
      return Result;
    }

    /// <summary>
    /// Searches the defined attribute and returns the value
    /// </summary>
    /// <param name="Attribute">wanted attribute</param>
    /// <returns></returns>
    public string ReadAttributeValue(string Attribute)
    {
      int Rewind = Position;
      string ReadAttribute = string.Empty;
      string Value = string.Empty;

      while(XML[Position] != '>')
      {
        ReadAttribute += XML[Position++];
        if (XML[Position] == '=')
        {
          if (ReadAttribute == Attribute)
          {
            Value = ReadValue();
            break;
          }
          else
          {
            // if the found attribute isn't the wanted the memory of Readattribute needs to be cleaned
            ReadAttribute = string.Empty;
            // wind to next attribute
            ReadValue();
            // jump over the ' ' blank
            Position += 2; 
          }
        }
      }

      Position = Rewind;
      return Value;
    }

    private string ReadValue()
    {
      // +2 to jumpt over " sign 
      Position += 2;
      string Value = string.Empty;

      // Read Value
      while(XML[Position] != '"')
        Value += XML[Position++];

      return Value;
    }
  }
}
