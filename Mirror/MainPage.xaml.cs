﻿using System;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading.Tasks;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Mirror
{
  public sealed partial class MainPage : Page
  {
    DispatcherTimer timer = new DispatcherTimer();
    Weather WeatherInstance;
    static Image TransferImage;
    static ListView Calender;
    

    public MainPage()
    {
      this.InitializeComponent();
      TransferImage = WeatherImage;
      Calender = CalenderList;


      InitWeather();
      TimerInit();

      Uri grUri = new Uri(this.BaseUri, "/Assets/grammar.xml");
      SpeechRecog recognizer = new SpeechRecog(grUri,Dispatcher);

      Task task = new Task(recognizer.start); // Thread for SpeechRecognition
      task.Start();
    }

    private void InitWeather()
    {
      WeatherInstance = Weather.GetInstance();
      // First time we need to download it synchron
      
      weatherDataContainer weatherData = WeatherInstance.getWeatherData(true).GetAwaiter().GetResult();
      WeatherImage.Source = new BitmapImage(new Uri(this.BaseUri, weatherData.picture));
      Temperature.Text = weatherData.Temperature;
      City.Text = weatherData.City;

      // Now we set a period of an hour
      TimeSpan period = TimeSpan.FromHours(1);
      ThreadPoolTimer PeriodicWeather = ThreadPoolTimer.CreatePeriodicTimer(ReadWeather, period);
      // It now wil read the weather every hour asynchron
    }

    private async void ReadWeather(ThreadPoolTimer timer)
    {
      // Read Temperature,City and weather
      weatherDataContainer weatherData = await WeatherInstance.getWeatherData(false);  
         
      // Callback to the GUI Thread
      await Dispatcher.RunAsync(CoreDispatcherPriority.High, 
        () =>
        {
          WeatherImage.Source = new BitmapImage(new Uri(this.BaseUri, weatherData.picture));
          Temperature.Text = weatherData.Temperature;
          City.Text = weatherData.City;
        });
    }

    private void TimerInit()
    {
      timer.Interval = new TimeSpan(0, 0, 1);
      timer.Tick += Timer_Tick;
      timer.Start();
    }

    private void Timer_Tick(object sender, object e)
    {
      Time.Text = DateTime.Now.ToString("h:mm:ss");
    }

    public static void SetWeatherCollapsed()
    {
      TransferImage.Visibility = Visibility.Collapsed;
    }

    public static void SetWeatherVisible()
    {
        TransferImage.Visibility = Visibility.Visible;
    }

    public static void ChangeCalenderVisibility()
    {
      if (Calender.Visibility == Visibility.Visible)
        Calender.Visibility = Visibility.Collapsed;
      else
        Calender.Visibility = Visibility.Visible;
    }
  }
}
