﻿using System;
using System.Threading.Tasks;
using Windows.Web.Http;
using System.Diagnostics;

namespace Mirror
{
  public struct weatherDataContainer
  {
    public string Temperature;
    public string Condition;
    public string City;
    public string Country;
    public string Code;
    public string picture;
  }
  /// <summary>
  /// Class is a Singleton
  /// </summary>
  class Weather
  {
    string woeid = "684946";
    weatherDataContainer Data = new weatherDataContainer();
    Uri uri;
    static Weather Instance;

    private Weather()
    {
      String URL = String.Format(@"http://query.yahooapis.com/v1/public/yql?format=xml&q=select+*+from+weather.forecast+where+woeid=");   // URL um die Daten zu Laden
      URL = URL + woeid;
      // In Uri wandeln
      uri = new Uri(URL);
    }

    public static  Weather GetInstance()
    {
      if (Instance == null)
        Instance = new Weather();

      return Instance;
    }

    public async Task<weatherDataContainer> getWeatherData(bool awaiter)
    {
      HttpClient Client = new HttpClient();

      string DataString;

      try
      {
        if (awaiter == true)
          DataString = Client.GetStringAsync(uri).GetAwaiter().GetResult();
        else
          DataString = await Client.GetStringAsync(uri);

        XMLstringRead Reader = new XMLstringRead(DataString);

        Data.Country = Reader.GetAttfromElem("yweather:location", "country");
        Data.City = Reader.ReadAttributeValue("city");
        Data.Temperature = FahrenheitToCelsius(Reader.GetAttfromElem("yweather:condition", "temp"));
        Data.Condition = Reader.ReadAttributeValue("text");
        Data.Code = Reader.ReadAttributeValue("code");
        Data.picture = DefineIconPath(Data.Code);
      }
      catch (Exception ex)
      {
        Debug.WriteLine(ex.Message);
        Debug.WriteLine(ex.StackTrace);
      }    
      return Data;
    }

    private string FahrenheitToCelsius(string Fahrenheit)
    {
      double x = (Double.Parse(Fahrenheit) - 32) * 5.0 / 9.0;
      x = (int)x;
      return x.ToString() + " " + string.Format("\u00B0") + "C";
    }

    public string GetTemperature()
    {
      return Data.Temperature;
    }

    private string DefineIconPath(string Code)
    {
      string path;
      if (Code.Equals("3200"))
        path = "/Assets/WeatherIcons/na.png";
      else
        path = "/Assets/WeatherIcons/" + Code +".png";
      
      return path;
    }
  }
}