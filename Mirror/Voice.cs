﻿using System;
using System.Linq;
using Windows.Media.SpeechSynthesis;
using Windows.UI.Xaml.Controls;
using Mirror;

namespace AudioFeedback
{
    class Voice
    {
        MediaElement Speaker;
        SpeechSynthesizer Synthesizer;
        string WeatherSentence = "Die Temperatur beträgt ";

        public Voice()
        {
            Speaker = new MediaElement();
            Synthesizer = new SpeechSynthesizer();

            //VoiceInformation Voice = new VoiceInformation();
            var Voice = SpeechSynthesizer.AllVoices;
            Synthesizer.Voice = Voice.First( x => x.Gender == VoiceGender.Male && x.Language.Contains("de-DE"));
        }

        public void Speak(string Sentence)
        {
           // Generate the audio stream from plain text.
           SpeechSynthesisStream stream = Synthesizer.SynthesizeTextToStreamAsync(Sentence).GetAwaiter().GetResult();

           // Send the stream to the media object.
           Speaker.SetSource(stream, stream.ContentType);
           Speaker.Play();
        }

        public void SayWeather()
        {
            Weather WeatherInstance = Weather.GetInstance();
            Speak(WeatherSentence + WeatherInstance.GetTemperature());
        }

        public void SetVolumeInScales(int Gain)
        {
            if (Gain > 10)
                Gain = 10;
            Speaker.Volume = (Gain / 10);
        }

        public void SetVolume(double Gain)
        {
            if (Gain > 1.0)
                Gain = 1.0;
            Speaker.Volume = Gain;
        }

        public void VolumeUp()
        {
            if (Speaker.Volume >= 0.9)
                Speaker.Volume += 0.1;
        }

        public void Mute()
        {
            Speaker.Volume = 0.0;
        }

        public void VolumeDown()
        {
            if (Speaker.Volume >= 0.1)
                Speaker.Volume -= 0.1;
        }
    }
}

