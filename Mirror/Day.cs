﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;

namespace CalenderContainer
{
    class CalenderEntry
    {
        public string Title { get; set; }
        public string Location { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Description { get; set; }

        public CalenderEntry(string Title,string Location,string StartTime,string EndTime,string  Description)
        {
            this.Title = Title;
            this.Location = Location;
            this.StartTime = StartTime;
            this.EndTime = EndTime;
            this.Description = Description;
        }

        public CalenderEntry(XElement XEntry)
        {
            Title = (string)XEntry.Attribute("Title");
            Location = (string)XEntry.Attribute("Location");
            StartTime = (string)XEntry.Attribute("StartTime");
            EndTime = (string)XEntry.Attribute("EndTime");
            Description = (string)XEntry.Attribute("Description");
        }

        public XElement ToXElement()
        {
            XElement XEntry = new XElement(new XElement("Entry",
                new XAttribute("Title", Title),
                new XAttribute("Location", Location),
                new XAttribute("StartTime", StartTime),
                new XAttribute("EndTime", EndTime),
                new XAttribute("Description", Description)));

            return XEntry;
        }
    }

    class Day : IEnumerable<CalenderEntry>
    {
        List<CalenderEntry> DayEntrys;
        public string Date { get; set; }

        public Day()
        {
            Date = DateTime.Today.ToString();
            DayEntrys = new List<CalenderEntry>();
        }

        public Day(string Date)
        {
            this.Date = Date;
            DayEntrys = new List<CalenderEntry>();
        }

        public Day(CalenderEntry Entry ,string Date)
        {
            this.Date = Date;
            DayEntrys = new List<CalenderEntry>();
            DayEntrys.Add(Entry);
        }

        public Day(List<CalenderEntry> Entrys, string Date)
        {
            this.Date = Date;
            DayEntrys = new List<CalenderEntry>();
            DayEntrys.AddRange(Entrys);
        }

        public Day(XElement XDay)
        {
            DayEntrys = new List<CalenderEntry>();
            IEnumerable<XElement> XEntrys = XDay.Elements("Entry");
            foreach(XElement Entry in XEntrys)
            {
                DayEntrys.Add(new CalenderEntry(Entry));
            }
        }


        public XElement ToXElement()
        {
            // Date wird als Key benutzt
            XElement XEntry = new XElement("Day", new XAttribute("Date",Date));
            // Write all Entrys out of the Day
            foreach(CalenderEntry Entry in DayEntrys)
                XEntry.Add(Entry.ToXElement());

            return XEntry;
        }

        public void Add(CalenderEntry Entry)
        {
            DayEntrys.Add(Entry);
        }

        public void AddRange(List<CalenderEntry> Entrys)
        {
            DayEntrys.AddRange(Entrys);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<CalenderEntry>)DayEntrys).GetEnumerator();
        }

        IEnumerator<CalenderEntry> IEnumerable<CalenderEntry>.GetEnumerator()
        {
            return ((IEnumerable<CalenderEntry>)DayEntrys).GetEnumerator();
        }

        IEnumerator<CalenderEntry> GetEnumerator()
        {
            return ((IEnumerable<CalenderEntry>)DayEntrys).GetEnumerator();
        }
    }

}